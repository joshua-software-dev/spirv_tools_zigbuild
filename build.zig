const builtin = @import("builtin");
const std = @import("std");

const header_generator = @import("spirv_headers_generator");
pub const get_spirv_tools_git_hash = header_generator.get_spirv_tools_git_hash;


const DEFINE = struct {
    name: []const u8,
    value: []const u8,
};

fn add_spirv_tools_source_files(
    b: *std.Build,
    lib: *std.Build.Step.Compile,
    spirv_tools_relpath: []const u8,
    flags: []const []const u8,
    enable_spirv_opt: bool,
) !void {
    var spirv_tools_dir = try b.build_root.handle.openDir(spirv_tools_relpath, .{});
    defer spirv_tools_dir.close();

    const sub_dirs: []const []const u8 = switch (enable_spirv_opt) {
        true => &.{
            "source/",
            "source/util/",
            "source/val/",
            "source/opt/",
        },
        else => &.{
            "source/",
            "source/util/",
            "source/val/",
        },
    };

    for (sub_dirs) |sub_dir_path| {
        var sub_dir_fd = spirv_tools_dir.openDir(sub_dir_path, .{ .iterate = true })
            catch |err| switch (err) {
                error.FileNotFound => continue,
                else => return err,
            };
        defer sub_dir_fd.close();

        var iter = sub_dir_fd.iterateAssumeFirstIteration();
        while (try iter.next()) |entry| {
            switch (entry.kind) {
                .file => if (std.mem.endsWith(u8, entry.name, ".cpp")) {
                    lib.addCSourceFile(.{
                        .file = b.path(b.pathJoin(&.{ spirv_tools_relpath, sub_dir_path, entry.name })),
                        .flags = flags,
                    });
                },
                else => {},
            }
        }
    }
}

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const enable_opt =
        if (b.option(
            bool,
            "enable_opt",
            "Enable the spirv-tools optimizer, must be enabled to generate valid SPIRV from HLSL, default=true",
        )) |opt|
            opt
        else
            true;

    const enable_ubsan =
        if (b.option(
            bool,
            "enable_ubsan",
            "Enable the Undefined Behavior Sanitizer, default=false",
        )) |opt|
            opt
        else
            false;


    const maybe_spirv_headers_path = b.option(
        []const u8,
        "spirv_headers_path",
        "Path to a SPIRV-Headers repo location to use when generating files",
    );

    const spirv_tools_git_hash_override = b.option(
        []const u8,
        "spirv_tools_git_hash_override",
        "Override the result of the git hash lookup made when generating files with this string",
    )
        orelse "";

    const maybe_spirv_tools_path = b.option(
        []const u8,
        "spirv_tools_path",
        "Path to a SPIRV-Tools repo location to use when generating files",
    );

    const warnings_as_errors =
        if (b.option(
            bool,
            "warnings_as_errors",
            "Enable to treat all compiler warnings into errors (-Werror), default=true",
        )) |opt|
            opt
        else
            true;

    if (maybe_spirv_headers_path == null or maybe_spirv_tools_path == null) {
        return error.OptionRequired;
    }
    const spirv_headers_abspath = maybe_spirv_headers_path.?;
    const spirv_tools_abspath = maybe_spirv_tools_path.?;

    const spirv_headers_generator_dep = b.dependency("spirv_headers_generator", .{
        .target = target,
        .optimize = optimize,
        .spirv_headers_path = spirv_headers_abspath,
        .spirv_tools_path = spirv_tools_abspath,
        .spirv_tools_git_hash_override = spirv_tools_git_hash_override,
    });

    const spirv_headers_relpath = try std.fs.path.relative(
        b.allocator,
        b.build_root.path orelse return error.InvalidBuildRoot,
        spirv_headers_abspath,
    );
    defer b.allocator.free(spirv_headers_relpath);
    const spirv_tools_relpath = try std.fs.path.relative(
        b.allocator,
        b.build_root.path orelse return error.InvalidBuildRoot,
        spirv_tools_abspath,
    );
    defer b.allocator.free(spirv_tools_relpath);

    const spirv_tools_os_name = switch (target.result.os.tag) {
        .emscripten => "SPIRV_EMSCRIPTEN",
        .freebsd, .kfreebsd => "SPIRV_FREEBSD",
        .fuchsia => "SPIRV_FUCHSIA",
        .hurd => "SPIRV_GNU",
        .ios => "SPIRV_IOS",
        .linux => "SPIRV_LINUX",
        .macos => "SPIRV_MAC",
        .openbsd => "SPIRV_OPENBSD",
        .tvos => "SPIRV_TVOS",
        .wasi => "SPIRV_LINUX",
        .windows => "SPIRV_WINDOWS",
        else => @panic("Unsupported OS"),
    };

    const defines: []const DEFINE = switch (target.result.os.tag) {
        .linux => &.{
            .{
                .name = spirv_tools_os_name,
                .value = "1",
            },
            .{
                .name = "ENABLE_HLSL",
                .value = "1",
            },
            .{
                .name = "SPIRV_TIMER_ENABLED",
                .value = "1",
            },
        },
        else => &.{
            .{
                .name = spirv_tools_os_name,
                .value = "1",
            },
            .{
                .name = "ENABLE_HLSL",
                .value = "1",
            },
        },
    };

    var flags = blk: {
        var flag_array_list = std.ArrayList([]const u8).init(b.allocator);
        try flag_array_list.appendSlice(&.{
            "-std=c++17",
            "-Wall",
            "-Wuninitialized",
            "-Wunused",
            "-Wunused-local-typedefs",
            "-Wunused-parameter",
            "-Wunused-value",
            "-Wunused-variable",
            "-fno-rtti",
            "-fno-exceptions",
        });

        if (!enable_ubsan) {
            try flag_array_list.append("-fno-sanitize=undefined");
        }

        if (warnings_as_errors) {
            try flag_array_list.append("-Werror");
        }

        break :blk flag_array_list;
    };
    defer flags.deinit();

    const opt_flags = blk: {
        var flag_set = std.StringArrayHashMap(void).init(b.allocator);
        defer flag_set.deinit();

        for (flags.items) |flag| {
            try flag_set.put(flag, {});
        }

        const temp_flags: []const []const u8 = &.{
            "-Wextra-semi",
            "-Wextra",
            "-Wnon-virtual-dtor",
            "-Wno-missing-field-initializers",
            "-Wno-self-assign",
            "-Wno-long-long",
            "-Wshadow",
            "-Wundef",
            "-Wconversion",
            "-Wno-sign-conversion",
            "-ftemplate-depth=1024",
        };

        for (temp_flags) |flag| {
            try flag_set.put(flag, {});
        }

        var flag_array_list = std.ArrayList([]const u8).init(b.allocator);
        try flag_array_list.appendSlice(flag_set.keys());

        break :blk flag_array_list;
    };
    defer opt_flags.deinit();

    const spirv_tools = b.addStaticLibrary(.{
        .name = "SPIRV-Tools",
        .target = target,
        .optimize = optimize,
    });
    spirv_tools.root_module.pic = true;
    spirv_tools.linkLibCpp();
    for (defines) |define| {
        spirv_tools.defineCMacro(define.name, define.value);
    }

    spirv_tools.linkLibrary(spirv_headers_generator_dep.artifact("spirv_headers_generated"));
    spirv_tools.addIncludePath(b.path(spirv_tools_relpath));
    spirv_tools.addIncludePath(b.path(b.pathJoin(&.{ spirv_tools_relpath, "include" })));
    spirv_tools.addIncludePath(b.path(b.pathJoin(&.{ spirv_headers_relpath, "include" })));

    try add_spirv_tools_source_files(
        b,
        spirv_tools,
        spirv_tools_relpath,
        opt_flags.items,
        enable_opt,
    );

    // This declares intent for the executable to be installed into the
    // standard location when the user invokes the "install" step (the default
    // step when running `zig build`).
    b.installArtifact(spirv_tools);
}
