# spirv_tools_zigbuild

SPIRV-Tools packaged for the zig build system that doesn't require python or pre-generated files.

```zig
pub fn build(b: *std.Build) !void {
    // ...

    const spirv_headers_dep = b.dependency("spirv_headers", .{});
    const spirv_tools_dep = b.dependency("spirv_tools", .{});
    const spirv_tools_zigbuild_dep = b.dependency("spirv_tools_zigbuild", .{
        .target = target,
        .optimize = optimize,
        // Enable the spirv-tools optimizer, must be enabled to generate valid
        // SPIR-V from HLSL, default=true
        .enable_opt = true,
        // Enable the Undefined Behavior Sanitizer, default=false
        .enable_ubsan = false,
        .spirv_headers_path = @as([]const u8, spirv_headers_dep.path("").getPath(b)),
        .spirv_tools_path = @as([]const u8, spirv_tools_dep.path("").getPath(b)),
        // When using the zig package manager with SPIRV-Tools, it will not
        // include the .git folder in the cloned repo. One of the generator
        // scripts runs git commands to obtain the current hash of the repo,
        // and these commands will fail without that folder and its metadata
        // being present. You can specify the commit manually here, or let it
        // fallback as usual to the default of: "unknown hash".
        .spirv_tools_git_hash_override = @as([]const u8, "51892874ba08f3ac0d9b1fcf3893c8516693a88e"),
        // Enable to treat all compiler warnings into errors (-Werror), default=true
        .warnings_as_errors = true,
    });

    const my_exe = b.addExecutable(...);
    my_exe.linkLibrary(spirv_tools_zigbuild_dep.artifact("SPIRV-Tools"));

    // ...
}
```
